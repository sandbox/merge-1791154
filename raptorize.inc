<?php

function raptorize_post() {

	// Ensure content was posted
	if (!isset($_POST['raptor-content'])) {
	    return 'No content to save';
	}
	 
	// Decode content
	$postedContent = json_decode($_POST['raptor-content']);
	 
	// Validate content
	if (!$postedContent) {
	    if (json_last_error() !== JSON_ERROR_NONE) {
	        return 'Error reading content, please try again';
	    }
	    return 'No content to save';
	}
	
	/*
	$debug = "<pre>";
	$debug .= print_r($_POST['raptor-content'],1);
	$debug .= "</pre>";
	watchdog('raptorize', $debug, NULL, WATCHDOG_DEBUG);
  */

	// Save content
	foreach ($postedContent as $id => $html) {

		// @TODO this is just ugly...
		// $id = data-field-id = ENTITY_TYPE - NID - FIELD_NAME
		//Get the id of your field
		$data = explode('-', $id);

  	$node = node_load($data[1]);
  	if (field_access('edit', $data[2], $node->type) === TRUE) {
	  	$node->{$data[2]}[LANGUAGE_NONE][0]['value'] = $html;
			field_attach_presave($data[0], $node);
			field_attach_update($data[0], $node);
		} else {
			return "Access denied.";
		}
	}

}