DESCRIPTION:

This module will enable frontend, inline editting using the HTML5 Raptor editor.
Raptor is a HTML5, JQuery UI enabled editor with a GPL licens. 
See http://www.raptor-editor.com for details. It's currently under heavy development.

INSTALL:

1. download VERSION 0.0.27 from raptor-editor.com and place raptor.min.js in the /js/ module folder.

for example: /sites/all/modules/raptorize/js/raptor.min.js

2. make sure you use JQUERY version 1.7 using the jquery_update module

settings are available at admin/config/development/jquery_update

3. Make sure you have the permission checked to use the editor.

4. Create a node. Goto the full node and hover over the body. You should see a message "Click here to edit".


NOTES:

This module is mostly a proof of concept, needs testing.
See various @TODO markers in code.

Made by Albert Skibinski (@askibinski) - www.merge.nl

- only node fields are supported
- node title field not yet supported
- assumes all fields are (filtered)html
- only works in the frontend
- bug with dissapearing content after save (page refresh needed)
- probably won't work with Panels, DS, or any other custom display formatters
- editor not yet customisable (needs settings)