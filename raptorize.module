<?php
/**
 * This module is mostly a proof of concept, needs testing.
 * see various @TODO markers
 * Made by Albert Skibinski (@askibinski) - www.merge.nl
 * - only node fields are supported
 * - node title field not yet supported
 * - assumes all fields are (filtered)html
 * - only works in the frontend
 * - probably won't work with Panels, DS, or any other custom display formatters
 */


/**
 * Implements hook_node_view().
 */
function raptorize_node_view($node, $view_mode, $langcode) {
  // if this is fired, we are apparantly looking at a node
    $editable_fields = raptorize_check_access($node);
    if ($editable_fields !== FALSE) {
      // @TODO we probably don't need *every* ui component...
      drupal_add_library('system', 'ui');
      drupal_add_library('system', 'ui.accordion');
      drupal_add_library('system', 'ui.autocomplete');
      drupal_add_library('system', 'ui.button');
      drupal_add_library('system', 'ui.datepicker');
      drupal_add_library('system', 'ui.dialog');
      drupal_add_library('system', 'ui.draggable');
      drupal_add_library('system', 'ui.droppable');
      drupal_add_library('system', 'ui.mouse');
      drupal_add_library('system', 'ui.position');
      drupal_add_library('system', 'ui.progressbar');
      drupal_add_library('system', 'ui.resizable');
      drupal_add_library('system', 'ui.selectable');
      drupal_add_library('system', 'ui.slider');
      drupal_add_library('system', 'ui.sortable');
      drupal_add_library('system', 'ui.tabs');
      drupal_add_library('system', 'ui.widget');
      drupal_add_js(drupal_get_path('module', 'raptorize') .'/js/raptorize.js');
      drupal_add_js(drupal_get_path('module', 'raptorize') .'/js/raptor.min.js');

      // pass variables to javascript
      // @TODO we don't really use these vars in js at this point, because all editable fields 
      // are given a class 'raptorize' by our custom theme_field function below.
      // however, this is not fool-proof. Users might use their own theme_field or 
      // something like panels or DS...

      // @TODO text formats
      // also, the field may be in plain text, which the editor is unawere about
      $js_vars = array(
      'raptorize' => array(
        'editable_fields' => implode(',', $editable_fields),
        ),
      );
      drupal_add_js($js_vars, 'setting');
    }
}

/**
 * Returns an array of editable fields for current node or false if none or no permissions
 */
function raptorize_check_access($node) {
  if (user_access("raptor inline editting") && node_access("update", $node) === TRUE) {

    $bundle = 'node'; // we only do nodes for now..
    $fields_info = field_info_instances($bundle, $node->type);

    foreach ($fields_info as $field_name => $value) {
      $field = field_info_field($field_name);
      if (field_access('edit', $field, $node->type) === TRUE) {
        $editable_fields[] = $field_name;
      }
    }
    // at this point, we know which fields are in the node type we're looking at,
    // but we can't be sure they are actually displayed
    return $editable_fields;
  } else {
    return false;
  }

}

/**
 * Implements hook_menu().
 */
function raptorize_menu() {
  $items['raptorize'] = array(
    'title' => 'Raptorize',
    'page callback' => 'raptorize_post',
    'access arguments' => array('Raptor inline editting'),
    'type' => MENU_CALLBACK,
    'file' => 'raptorize.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function raptorize_permission() {
  return array(
    'raptor inline editting' =>  array(
      'title' => t('Raptor inline editting'),
      'description' => t('Allow to use the inline HTML5 editor.'),
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function raptorize_theme_registry_alter(&$theme_registry) {
  // overriding core theme_field function to include data attribute
  $theme_registry['field']['function'] = 'raptorize_field';
}

/**
 * customized version of theme_field (added $data_attr and inject .raptorize class)
 */
function raptorize_field($variables) {

  $editable_fields = raptorize_check_access($variables['element']['#object']);

  $output = '';

  $data_attr = $variables['element']['#entity_type'] . '-' . $variables['element']['#object']->nid . '-' . $variables['element']['#field_name'];

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    if (in_array($variables['element']['#field_name'], $editable_fields)) {
      $output .= '<div class="' . $classes . ' raptorize"' . $variables['item_attributes'][$delta] . ' data-field-id="' . $data_attr . '">' . drupal_render($item) . '</div>';
    } else {
      $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] .'">' . drupal_render($item) . '</div>';
    }
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

return $output;
}
