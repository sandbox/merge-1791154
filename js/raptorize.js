(function ($) {

  Drupal.raptorize = {}
  
  Drupal.behaviors.raptorize = {
    attach: function(context, settings) {

    console.log(settings);

		// docs see: https://github.com/PANmedia/Raptor/issues/38

    // example json save: http://www.raptor-editor.com/demo/save-json

          $('.raptorize').editor({
              plugins: {

		
					        dock: {
					            docked: true,
					            dockToElement: true
					        },

                  saveJson: {
                      /**
                       * True if you want a message to be displayed to the user on save
                       */
                      showResponse: true,
                      /**
                       * Attribute name that contains the content's ID.
                       * Instead of what is used here, you may also provide a string or a function.
                       * The function would be called with the editing element as the first argument,
                       * and the saveJson plugin as the context (this).
                       */
                      id: {
                          attr: 'data-field-id'
                      },
                      /**
                       * String to be used as the editing element's content key.
                       * e.g. $_POST['raptor-content']
                       */
                      postName: 'raptor-content',
                      ajax: {
                          url: '/raptorize'
                      }
                  }
              }
          });


    }
  }
  
}(jQuery));